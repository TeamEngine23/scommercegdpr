<?php


namespace Engine23\ScommerceGdpr\Test\Unit\Plugin\Helper;

use Engine23\ScommerceGdpr\Helper\Data as ConfigHelper;
use Engine23\ScommerceGdpr\Plugin\Helper\Data;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
    /**
     * @var ConfigHelper
     */
    protected $mockConfigHelper;

    protected function setUp(): void
    {
        $this->objectManager    = new ObjectManager($this);
        $this->mockConfigHelper = $this->createMock(ConfigHelper::class);
    }

    public function testAfterIsEnabledNotSuppressed()
    {
        $this->mockConfigHelper->method('isSuppressedForNonGdprUsers')->willReturn(false);
        $this->mockConfigHelper->method('isGdprRequired')->willReturn(false);
        $testDataPlugin = $this->objectManager->getObject(
            Data::class,
            [
                'configHelper'  => $this->mockConfigHelper,
            ]
        );
        $this->assertTrue(
            $testDataPlugin->afterIsEnabled($testDataPlugin, true),
            'GDPR should not be suppressed if the flag is not set.'
        );
    }

    public function testAfterIsEnabledForGdpr()
    {
        $this->mockConfigHelper->method('isSuppressedForNonGdprUsers')->willReturn(true);
        $this->mockConfigHelper->method('isGdprRequired')->willReturn(true);
        $testDataPlugin = $this->objectManager->getObject(
            Data::class,
            [
                'configHelper'  => $this->mockConfigHelper,
            ]
        );

        $this->assertTrue(
            $testDataPlugin->afterIsEnabled($testDataPlugin, true),
            'GDPR should not be suppressed for GDPR Users'
        );
    }

    public function testAfterIsEnabledForNonGdpr()
    {
        $this->mockConfigHelper->method('isSuppressedForNonGdprUsers')->willReturn(true);
        $this->mockConfigHelper->method('isGdprRequired')->willReturn(false);
        $testDataPlugin = $this->objectManager->getObject(
            Data::class,
            [
                'configHelper'  => $this->mockConfigHelper,
            ]
        );
        $this->assertFalse(
            $testDataPlugin->afterIsEnabled($testDataPlugin, true),
            'GDPR should be suppressed for non-GDPR Users'
        );
    }
}

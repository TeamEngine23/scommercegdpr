<?php


namespace Engine23\ScommerceGdpr\Test\Unit\Plugin\Block;

use Engine23\ScommerceGdpr\Plugin\Block\Script;
use Engine23\ScommerceGdpr\Helper\Data as ConfigHelper;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

class ScriptTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
    /**
     * @var ConfigHelper
     */
    protected $mockConfigHelper;

    protected function setUp(): void
    {
        $this->objectManager    = new ObjectManager($this);
        $this->mockConfigHelper = $this->createMock(ConfigHelper::class);
    }

    public function testAfterGetJsConfigParamsGdprRequired()
    {
        $this->mockConfigHelper->method('isGdprRequired')->willReturn(true);
        /** @var Script $testScriptBlock */
        $testScriptBlock = $this->objectManager->getObject(
            Script::class,
            [
                'configHelper' => $this->mockConfigHelper,
            ]
        );

        $this->assertArrayHasKey('is_gdpr_required', $testScriptBlock->afterGetJsConfigParams());
        $this->assertEquals(true, $testScriptBlock->afterGetJsConfigParams()['is_gdpr_required']);
    }

    public function testAfterGetJsConfigParamsGdprNotRequired()
    {
        $this->mockConfigHelper->method('isGdprRequired')->willReturn(false);
        /** @var Script $testScriptBlock */
        $testScriptBlock = $this->objectManager->getObject(
            Script::class,
            [
                'configHelper' => $this->mockConfigHelper,
            ]
        );

        $this->assertArrayHasKey('is_gdpr_required', $testScriptBlock->afterGetJsConfigParams());
        $this->assertEquals(false, $testScriptBlock->afterGetJsConfigParams()['is_gdpr_required']);
    }
}

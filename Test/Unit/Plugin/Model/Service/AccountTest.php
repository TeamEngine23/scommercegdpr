<?php

namespace Engine23\ScommerceGdpr\Test\Unit\Plugin\Model\Service;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Exception\MailException;
use PHPUnit\Framework\TestCase;


class AccountTest extends TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    protected $mockCustomer;
    protected $mockConfigHelper;
    protected $mockEmailHelper;
    protected $mockAccountService;
    protected $mockCustomerFactory;
    protected $mockCustomerModel;

    protected function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->mockCustomer = $this->createMock(\Magento\Customer\Model\Data\Customer::class);

        $this->mockConfigHelper = $this->createMock(\Engine23\ScommerceGdpr\Helper\Data::class);
        $this->mockConfigHelper->method('sendEmailToSiteAdminIsEnabled')->willReturn(true);
        $this->mockConfigHelper->method('getSiteAdminEmailAddress')->willReturn('test@test.com');
        $this->mockConfigHelper->method('getGdprAdminNoticeEmailTemplate')->willReturn('test_email_template');

        $this->mockEmailHelper = $this->createMock(\Scommerce\Gdpr\Helper\Email::class);

        $this->mockAccountService = $this->createMock(\Scommerce\Gdpr\Model\Service\Account::class);

        $this->mockCustomerModel = $this->createMock(Customer::class);
        $this->mockCustomerModel->method('updateData')->willReturnSelf();
        $this->mockCustomerModel->method('getId')->willReturn('1000000001');
        $this->mockCustomerModel->method('getName')->willReturn('Test Customer');

        $this->mockCustomerFactory = $this->getMockBuilder(CustomerFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->mockCustomerFactory->method('create')->willReturn($this->mockCustomerModel);
    }

    public function testAroundAnonymize()
    {
        $this->mockAccountService->method('anonymize')->willReturn(true);

        $this->mockEmailHelper->method('send')->willReturn(null);

        $testAnonymizationPlugin = $this->objectManager->getObject(
            \Engine23\ScommerceGdpr\Plugin\Model\Service\Account::class,
            [
                'configHelper'  => $this->mockConfigHelper,
                'emailHelper'   => $this->mockEmailHelper,
                'customerFactory'   => $this->mockCustomerFactory
            ]
        );

        $proceed = [$this->mockAccountService, 'anonymize'];
        $this->assertTrue(
            $testAnonymizationPlugin->aroundAnonymize($this->mockAccountService, $proceed, $this->mockCustomer)
        );
    }

    public function testAroundAnonymizeHandleCustomerAnonymizationFailure()
    {
        $this->mockAccountService->method('anonymize')->willReturn(false);

        $this->mockEmailHelper->method('send')->willReturn(null);

        $testAnonymizationPlugin = $this->objectManager->getObject(
            \Engine23\ScommerceGdpr\Plugin\Model\Service\Account::class,
            [
                'configHelper'      => $this->mockConfigHelper,
                'emailHelper'       => $this->mockEmailHelper,
                'customerFactory'   => $this->mockCustomerFactory
            ]
        );

        $proceed = [$this->mockAccountService, 'anonymize'];
        $this->assertFalse(
            $testAnonymizationPlugin->aroundAnonymize($this->mockAccountService, $proceed, $this->mockCustomer)
        );
    }

    public function testAroundAnonymizeHandleEmailFailure()
    {
        $expectedResult = true;
        $this->mockAccountService->method('anonymize')->willReturn($expectedResult);

        $this->mockEmailHelper->method('send')
            ->willReturn(null)
            ->willThrowException(new MailException(__('Something went wrong.')));

        $testAnonymizationPlugin = $this->objectManager->getObject(
            \Engine23\ScommerceGdpr\Plugin\Model\Service\Account::class,
            [
                'configHelper'      => $this->mockConfigHelper,
                'emailHelper'       => $this->mockEmailHelper,
                'customerFactory'   => $this->mockCustomerFactory
            ]
        );

        //Plugin will catch mail exception, result from customer anonymization does not change
        $proceed = [$this->mockAccountService, 'anonymize'];
        $this->assertEquals(
            $expectedResult,
            $testAnonymizationPlugin->aroundAnonymize($this->mockAccountService, $proceed, $this->mockCustomer)
        );
    }

    public function testAroundAnonymizeWithUnsetEmailTemplate()
    {
        $expectedResult = true;

        $this->mockAccountService
            ->method('anonymize')
            ->willReturn($expectedResult);

        $this->mockEmailHelper
            ->method('send')
            ->willReturn(null)
            ->willThrowException(new \UnexpectedValueException);

        $testAnonymizationPlugin = $this->objectManager->getObject(
            \Engine23\ScommerceGdpr\Plugin\Model\Service\Account::class,
            [
                'configHelper'      => $this->mockConfigHelper,
                'emailHelper'       => $this->mockEmailHelper,
                'customerFactory'   => $this->mockCustomerFactory
            ]
        );

        $proceed = [$this->mockAccountService, 'anonymize'];
        $this->assertEquals(
            $expectedResult,
            $testAnonymizationPlugin->aroundAnonymize($this->mockAccountService, $proceed, $this->mockCustomer)
        );
    }

    public function testAroundAnonymizeGenericFailure()
    {
        $this->mockAccountService
            ->method('anonymize')
            ->willReturn(false)
            ->willThrowException(new \Exception('Something went wrong.'));

        $this->mockEmailHelper->method('send')->willReturn(null);

        $testAnonymizationPlugin = $this->objectManager->getObject(
            \Engine23\ScommerceGdpr\Plugin\Model\Service\Account::class,
            [
                'configHelper'      => $this->mockConfigHelper,
                'emailHelper'       => $this->mockEmailHelper,
                'customerFactory'   => $this->mockCustomerFactory
            ]
        );

        //Plugin will not catch generic exception
        $this->expectException(\Exception::class);
        $proceed = [$this->mockAccountService, 'anonymize'];
        $testAnonymizationPlugin->aroundAnonymize($this->mockAccountService, $proceed, $this->mockCustomer);
    }
}

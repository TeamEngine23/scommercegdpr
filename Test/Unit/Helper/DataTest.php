<?php


namespace Engine23\ScommerceGdpr\Test\Unit\Helper;

use Engine23\ScommerceGdpr\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
    /**
     * @var DirectoryList
     */
    protected $mockDirectoryList;
    /**
     * @var Request
     */
    protected $mockRequest;

    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);

        $this->mockDirectoryList = $this->createMock(DirectoryList::class);
        $this->mockDirectoryList->method('getRoot')->willReturn(
            realpath(dirname(__FILE__) . '/../../../../../../../')
        );

        $this->mockRequest = $this->createMock(Request::class);
    }

    public function ipProvider()
    {
        return [
           ['12.111.40.0', false], // US
           ['88.198.50.0', true], // DE
        ];
    }

    /**
     * @dataProvider ipProvider
     */
    public function testIsGdprRequired($ip, $expected)
    {
        $this->mockRequest->method('getClientIp')->willReturn($ip);
        $testDataHelper = $this->objectManager->getObject(
            Data::class,
            [
                'directoryList' => $this->mockDirectoryList,
                'request'       => $this->mockRequest,
            ]
        );

        $this->assertEquals(
            $testDataHelper->isGdprRequired(),
            $expected,
            $expected
            ? "GDPR should be required for IP {$ip}"
            : "GDPR should not be required for IP {$ip}"
        );
    }
}

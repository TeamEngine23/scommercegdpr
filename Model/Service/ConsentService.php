<?php

namespace Engine23\ScommerceGdpr\Model\Service;

use Magento\Framework\Exception\LocalizedException;
use Scommerce\Gdpr\Api\Data\ConsentInterface;

/**
 * Class ConsentService
 * @package Engine23\ScommerceGdpr\Model\Service
 */
class ConsentService extends \Scommerce\Gdpr\Model\Service\ConsentService
{
    /** @var \Magento\Store\Model\StoreManagerInterface */
    private $storeManager;

    /** @var \Magento\Framework\Api\SearchCriteriaBuilder */
    private $criteriaBuilder;

    /** @var \Magento\Framework\Api\SortOrderBuilder */
    private $orderBuilder;

    /** @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress */
    private $remoteAddress;

    /** @var \Scommerce\Gdpr\Api\ConsentRepositoryInterface */
    private $repository;

    /** @var \Scommerce\Gdpr\Model\ConsentFactory */
    private $consentFactory;

    /** @var \Scommerce\Gdpr\Helper\Data */
    private $helper;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder
     * @param \Magento\Framework\Api\SortOrderBuilder $orderBuilder
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Scommerce\Gdpr\Api\ConsentRepositoryInterface $repository
     * @param \Scommerce\Gdpr\Model\ConsentFactory $consentFactory
     * @param \Scommerce\Gdpr\Helper\Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder $orderBuilder,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Scommerce\Gdpr\Api\ConsentRepositoryInterface $repository,
        \Scommerce\Gdpr\Model\ConsentFactory $consentFactory,
        \Scommerce\Gdpr\Helper\Data $helper
    ) {
        parent::__construct($storeManager, $criteriaBuilder, $orderBuilder, $remoteAddress, $repository, $consentFactory, $helper);

        $this->storeManager = $storeManager;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->orderBuilder = $orderBuilder;
        $this->remoteAddress = $remoteAddress;
        $this->repository = $repository;
        $this->consentFactory = $consentFactory;
        $this->helper = $helper;
    }

    /**
     * Check if record with Checkout source is exists for current customer or for anonymous user by email
     *
     * @TODO: THIS IS A COPY+PASTE OF THE PARENT FUNCTION. UPDATE THIS FUNCTION WHEN SCOMMERCE GDPR UPDATES. CURRENT VERSION 1.0.6
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface|string $value
     * @return bool
     * @throws LocalizedException
     */
    public function isExistsCheckout($value)
    {
        return $this->isExists($this->getCheckoutKey(), $value);
    }

    /**
     * Check if record with specified source is exists for current customer or for anonymous user by email
     *
     * @TODO: THIS IS A COPY+PASTE OF THE PARENT FUNCTION. UPDATE THIS FUNCTION WHEN SCOMMERCE GDPR UPDATES. CURRENT VERSION 1.0.6
     *
     * @param string $source
     * @param \Magento\Customer\Api\Data\CustomerInterface|string $value
     * @return bool
     * @throws LocalizedException
     */
    private function isExists($source, $value)
    {
        $builder = $this->criteriaBuilder
            ->addFilter(ConsentInterface::WEBSITE_ID, $this->storeManager->getWebsite()->getId())
            ->addFilter(ConsentInterface::SOURCE, $source);
        if ($this->isCustomer($value)) {
            $builder->addFilter(ConsentInterface::CUSTOMER_ID, $value->getId());
        } else {
            $builder->addFilter(ConsentInterface::GUEST_EMAIL, (string) $value);
        }
        $collection = $this->repository->getList($builder->create());
        return $collection->getTotalCount() > 0;
    }

    /**
     * Check if specified value instance of Customer. Fix issue with parent customer test only checking for
     * instance of \Magento\Customer\Api\Data\CustomerInterface - check for Customer model as well
     *
     * @param mixed $value
     * @return bool
     */
    private function isCustomer($value)
    {
        return
            $value instanceof \Magento\Customer\Api\Data\CustomerInterface ||
            $value instanceof \Magento\Customer\Model\Customer;
    }

    /**
     * @TODO: THIS IS A COPY+PASTE OF THE PARENT FUNCTION. UPDATE THIS FUNCTION WHEN SCOMMERCE GDPR UPDATES. CURRENT VERSION 1.0.6
     * @return string
     */
    private function getCheckoutKey()
    {
        return $this->helper->getPrivacySourceHelper()->getCheckoutKey();
    }
}

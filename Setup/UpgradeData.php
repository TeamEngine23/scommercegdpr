<?php

namespace Engine23\ScommerceGdpr\Setup;

use Engine23\ScommerceGdpr\Helper\Data;
use Magento\Framework\Module\Manager;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Scommerce\CookiePopup\Model\ChoiceFactory;
use Scommerce\CookiePopup\Model\ChoiceRepository;

/**
 * Class UpgradeData
 *
 * @package Engine23\ScommerceGdpr\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Data
     */
    private $configHelper;
    /**
     * @var ChoiceFactory
     */
    private $choiceFactory;
    /**
     * @var ChoiceRepository
     */
    private $choiceRepository;
    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * UpgradeData constructor.
     *
     * @param Data $configHelper
     * @param ChoiceFactory $choiceFactory
     * @param ChoiceRepository $choiceRepository
     * @param Manager $moduleManager
     */
    public function __construct(
        Data $configHelper,
        ChoiceFactory $choiceFactory,
        ChoiceRepository $choiceRepository,
        Manager $moduleManager
    ) {
        $this->configHelper     = $configHelper;
        $this->choiceFactory    = $choiceFactory;
        $this->choiceRepository = $choiceRepository;
        $this->moduleManager    = $moduleManager;
    }

    /**
     * Upgrade data
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            if ($this->moduleManager->isEnabled('Scommerce_CookiePopup')) {
                $data   = [
                    'choice_name'        => 'Performance Cookies',
                    'choice_description' => 'These cookies collect information about how visitors use a website, for instance which pages visitors go to most often, and if they get error messages from web pages. These cookies don’t collect information that identifies a visitor. All information these cookies collect is aggregated and therefore anonymous. It is only used to improve how a website works.',
                    'cookie_name'        => 'performance_cookies',
                    'list'               => "Magento Store\nGoogle",
                    'required'           => 1,
                    'default_state'      => 1,
                    'stores'             => [0],
                ];
                $choice = $this->choiceFactory->create();
                $choice->addData($data);
                $this->choiceRepository->save($choice);

                $data   = [
                    'choice_name'        => 'Targeting & Advertising Cookies',
                    'choice_description' => 'These cookies are used to deliver adverts more relevant to you and your interests They are also used to limit the number of times you see an advertisement as well as help measure the effectiveness of the advertising campaign. They are usually placed by advertising networks with the website operator’s permission. They remember that you have visited a website and this information is shared with other organisations such as advertisers. Quite often targeting or advertising cookies will be linked to site functionality provided by the other organisation.',
                    'cookie_name'        => $this->configHelper->getAdvertisingCookieKey(),
                    'list'               => "Google\nFacebook\nBing",
                    'required'           => 0,
                    'default_state'      => 1,
                    'stores'             => [0],
                ];
                $choice = $this->choiceFactory->create();
                $choice->addData($data);
                $this->choiceRepository->save($choice);
            }
        }

        $setup->endSetup();
    }
}

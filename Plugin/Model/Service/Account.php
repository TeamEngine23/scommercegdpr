<?php

namespace Engine23\ScommerceGdpr\Plugin\Model\Service;

use Magento\Customer\Model\CustomerFactory;
use Scommerce\Gdpr\Model\Service\Account as AccountService;
use Scommerce\Gdpr\Helper\Email;
use Engine23\ScommerceGdpr\Helper\Data;
use Magento\Customer\Model\Data\Customer;

class Account
{
    /**
     * @var \Engine23\ScommerceGdpr\Helper\Data
     */
    protected $configHelper;

    /**
     * @var \Scommerce\Gdpr\Helper\Email
     */
    protected $emailHelper;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * Account constructor.
     * @param Data $configHelper
     * @param Email $emailHelper
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        Data $configHelper,
        Email $emailHelper,
        CustomerFactory $customerFactory
    ) {
        $this->configHelper     = $configHelper;
        $this->emailHelper      = $emailHelper;
        $this->customerFactory  = $customerFactory;
    }

    /**
     * Trigger email message to site admin when account is anonymized
     * @param AccountService $subject
     * @param callable $proceed
     * @param Customer $customer
     * @return mixed
     * @throws \Exception
     */
    public function aroundAnonymize(AccountService $subject, callable $proceed, Customer $customer)
    {
        //Gather customer var data before we anonymize it...
        $customerData = [];

        $customerData['name']       = $this->getCustomerName($customer);
        $customerData['customerId'] = $customer->getId();

        //Anonymize customer
        $result = $proceed($customer);

        //Trigger e-mail to site admin if enabled...
        if ($result) {
            $this->sendAdminEmail($customerData);
        }

        return $result;
    }

    /**
     * @param array $customerVarData
     * @throws \Exception
     */
    protected function sendAdminEmail(array $customerVarData)
    {
        if ($this->configHelper->isEnabled() && $this->configHelper->sendEmailToSiteAdminIsEnabled()) {
            $template           = $this->configHelper->getGdprAdminNoticeEmailTemplate();
            $sender             = $this->configHelper->getEmailSender();
            $receiverAddress    = $this->configHelper->getSiteAdminEmailAddress();

            $receiver           = ['email' => $receiverAddress];

            if (isset($customerVarData['name'])) {
                $receiver['name'] = $customerVarData['name'];
            }

            try {
                $this->emailHelper->send($template, $sender, $receiver, $customerVarData);
            } catch (\Magento\Framework\Exception\MailException $e) {
                //Do not change result from customer anonymization
            } catch (\UnexpectedValueException $e) {
                //Do not change result from customer anonymization
            }
        }
    }

    /**
     * @param Customer $customer
     * @return string
     */
    protected function getCustomerName(Customer $customer)
    {
        $customerModel = $this->customerFactory->create();
        $customerModel->updateData($customer);

        return $customerModel->getName();
    }
}

<?php


namespace Engine23\ScommerceGdpr\Plugin\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\HTTP\PhpEnvironment\Request;

/**
 * Class Data
 *
 * @package Engine23\ScommerceGdpr\Plugin\Helper
 */
class Data
{
    /**
     * @var \Engine23\ScommerceGdpr\Helper\Data
     */
    protected $configHelper;
    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var Request
     */
    protected $request;

    /**
     * Data constructor.
     *
     * @param \Engine23\ScommerceGdpr\Helper\Data $configHelper
     */
    public function __construct(
        \Engine23\ScommerceGdpr\Helper\Data $configHelper
    ) {
        $this->configHelper  = $configHelper;
    }

    /**
     * Overrides isEnabled function to check if the client comes from a GDPR country.
     *
     * @param \Scommerce\Gdpr\Helper\Data $subject
     * @param bool $result
     * @return bool
     */
    public function afterIsEnabled($subject, $result)
    {
        // Scommerce says the plugin is disabled, skip check
        if (!$result) {
            return $result;
        }

        // Flag is not set, skip check
        if (!$this->configHelper->isSuppressedForNonGdprUsers()) {
            return $result;
        }

        return $this->configHelper->isGdprRequired();
    }

}

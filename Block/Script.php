<?php

namespace Engine23\ScommerceGdpr\Block;

use Engine23\ScommerceGdpr\Helper\Data as ConfigHelper;
use Magento\Customer\Model\Session;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;


/**
 * Class Script
 *
 * @package Engine23\ScommerceGdpr\Block
 */
class Script extends Template
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;
    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Script constructor.
     *
     * @param Context $context
     * @param ConfigHelper $configHelper
     * @param JsonSerializer $jsonSerializer
     * @param CookieManagerInterface $cookieManager
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        JsonSerializer $jsonSerializer,
        CookieManagerInterface $cookieManager,
        Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->configHelper    = $configHelper;
        $this->jsonSerializer  = $jsonSerializer;
        $this->cookieManager   = $cookieManager;
        $this->customerSession = $customerSession;
    }

    /**
     * Retrieves extra GTM Data Layer parameters.
     *
     * @return string
     */
    public function getJsConfigParams()
    {
        $_params = [
            'gdpr_is_required'                  => $this->configHelper->isGdprRequired(),
            'gdpr_is_required_and_not_accepted' => $this->configHelper->isGdprRequired()
                && !$this->cookieManager->getCookie($this->configHelper->getAdvertisingCookieKey()),
            'gdpr_not_required_or_is_accepted'  => !$this->configHelper->isGdprRequired()
                || $this->cookieManager->getCookie($this->configHelper->getAdvertisingCookieKey())
        ];

        // Add customer to the data layer
        if ($this->customerSession->isLoggedIn()) {
            $_params['customer'] = [
                'email' => $this->customerSession->getCustomer()->getEmail(),
            ];
        }

        $param   = $this->sanitizeParams($_params);
        return $param;
    }

    /**
     * Converts params to sanitized JSON.
     *
     * @param array $params
     * @return mixed|string|string[]|null
     */
    private function sanitizeParams($params)
    {
        $param = preg_replace(
            '/"([^"]+)"s*:s*/',
            '$1: $2',
            $this->jsonSerializer->serialize($params)
        );

        $param = str_replace('",', '",' . chr(13), $param);
        $param = str_replace('],', '],' . chr(13), $param);
        $param = str_replace('},', '},' . chr(13), $param);
        $param = str_replace('{', '{' . chr(13), $param);
        $param = str_replace('}', chr(13) . '}', $param);
        return $param;
    }
}

<?php

namespace Engine23\ScommerceGdpr\Helper;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Store\Model\ScopeInterface;
use Scommerce\Gdpr\Helper\Anonymize;
use Scommerce\Gdpr\Helper\Email;
use Scommerce\Gdpr\Helper\Serializer;
use Scommerce\Gdpr\Helper\Source;
use Symfony\Component\HttpFoundation\IpUtils;

/**
 * Class Data
 *
 * @package Engine23\ScommerceGdpr\Helper
 */
class Data extends \Scommerce\Gdpr\Helper\Data
{
    const XML_ENABLE_SEND_TO_ADMIN        = 'scommerce_gdpr/general/enable_send_to_admin';
    const XML_SITE_ADMIN_EMAIL_ADDRESS    = 'scommerce_gdpr/general/site_admin_email_address';
    const XML_SITE_ADMIN_EMAIL_TEMPLATE   = 'scommerce_gdpr/general/admin_delete_notice_email_template';
    const XML_SUPPRESS_FOR_NON_GDPR_USERS = 'scommerce_gdpr/general/suppress_for_non_gdpr_users';

    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var Request
     */
    protected $request;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param Serializer $serializer
     * @param Email $email
     * @param Anonymize $anonymize
     * @param Source $source
     * @param \Scommerce\Core\Helper\Data $coreHelper
     * @param DirectoryList $directoryList
     * @param Request $request
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        Serializer $serializer,
        Email $email,
        Anonymize $anonymize,
        Source $source,
        \Scommerce\Core\Helper\Data $coreHelper,
        DirectoryList $directoryList,
        Request $request
    ) {
        parent::__construct($context, $customerFactory, $serializer, $email, $anonymize, $source, $coreHelper);
        $this->directoryList = $directoryList;
        $this->request       = $request;
    }

    /**
     * @return mixed
     */
    public function sendEmailToSiteAdminIsEnabled()
    {
        return $this->isSetFlag(self::XML_ENABLE_SEND_TO_ADMIN);
    }

    /**
     * @return mixed
     */
    public function getSiteAdminEmailAddress()
    {
        return $this->getValue(self::XML_SITE_ADMIN_EMAIL_ADDRESS);
    }

    /**
     * @return mixed
     */
    public function getGdprAdminNoticeEmailTemplate()
    {
        return $this->getValue(self::XML_SITE_ADMIN_EMAIL_TEMPLATE);
    }

    /**
     * @param string $path
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    protected function getValue($path, $scopeType = ScopeInterface::SCOPE_STORE, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($path, $scopeType, $scopeCode);
    }

    /**
     * @param string $path
     * @param string $scopeType
     * @param null $scopeCode
     * @return bool
     */
    protected function isSetFlag($path, $scopeType = ScopeInterface::SCOPE_STORE, $scopeCode = null)
    {
        return $this->scopeConfig->isSetFlag($path, $scopeType, $scopeCode);
    }

    /**
     * Checks if flag set to suppress GDPR functionality for non-GDPR users.
     *
     * @return boolean
     */
    public function isSuppressedForNonGdprUsers()
    {
        return $this->isSetFlag(self::XML_SUPPRESS_FOR_NON_GDPR_USERS);
    }

    /**
     * Determines if GDPR is required based on the client IP.
     *
     * @return bool
     */
    public function isGdprRequired()
    {
        return true;
        try {
            $rangesFile = $this->directoryList->getRoot() . '/vendor/woganmay/gdpr-blackhole/eu-ranges.txt';
            $ipRanges   = explode("\n", trim(file_get_contents($rangesFile)));
            return IpUtils::checkIp($this->request->getClientIp(), $ipRanges);
        } catch (FileSystemException $exception) {
            return true;
        }
    }

    /**
     * @return string
     */
    public function getAdvertisingCookieKey()
    {
        return 'advertising_cookies';
    }
}

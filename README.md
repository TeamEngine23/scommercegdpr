Scommerce GDPR Extension Enhancements
=====================================

Credits
-------
* Based on the following [Scommerce Mage Ltd.](https://www.scommerce-mage.com/) extensions:
  * [Magento 2 GDPR Compliance: Anonymisation of transaction data](https://www.scommerce-mage.com/magento-2-gdpr-compliance.html)
  * [Magento 2 Google Tag Manager (GTM) Enhanced Ecommerce Tracking](https://www.scommerce-mage.com/magento-2-google-tag-manager-enhanced-ecommerce-tracking.html)
* Enhanced by [Engine 23](https://www.engine23.com/): the eCommerce and Magento experts.
* With support from [BeadsOfCambay.com](https://www.beadsofcambay.com/), the premier distributor of jewelry making beads and jewelry supplies.
